library(pROC)
library(ggplot2) 
library(balance)
library(cowplot)
library(gridExtra)
library(data.table)
library(zCompositions)
library(NearestBalance)
source("cv_functions/cv_functions.R")

read_counts_table <- function(path){
  tab <- as.data.frame(fread(path))
  rownames(tab) <- tab[,1]
  tab[,1] <- NULL
  tab
}

make_short_names <- function(names){
  sapply(names, function(name){
    x <- strsplit(name, ';')[[1]]
    res <- x#[[1]]
    for (i in length(x):1){
      if (!grepl("unclassified",x[i])){
        res = paste0(x[i:length(x)], collapse = ";")
        break
      }
    }
    return(res)
  })
}

counts.28179361 <- read_counts_table("CD/data/counts_CD1_full_names.csv")
counts.29404425 <- read_counts_table("CD/data/counts_CD2_full_names.csv")
m <- fread("CD/data/metadata.csv", header = T)
counts <- read.csv("CD/data/counts.csv", header = T, row.names = 1)
counts <- counts[m$sample,]

colnames(counts.28179361) <- make_short_names(colnames(counts.28179361))
colnames(counts.29404425) <- make_short_names(colnames(counts.29404425))
metadata.28179361 <- fread("CD/data/metadata.csv")
metadata.29404425 <- fread("CD/data/metadata_CD2.csv")
metadata.29404425$study <- "pmid.29404425"

counts.29404425 <- counts.29404425[metadata.29404425$sample, ]
common_taxa <- intersect(colnames(counts.28179361),
                         colnames(counts.29404425))

counts.28179361 <- counts.28179361[metadata.28179361$sample, common_taxa]
counts.29404425 <- counts.29404425[metadata.29404425$sample, common_taxa]
tax <- colnames(counts.28179361)[
  colSums(counts.28179361>10) >= 0.5 * nrow(counts.28179361)]

abund.29404425 <- cmultRepl(counts.29404425[, tax])
abund.28179361 <- cmultRepl(counts.28179361[, tax])

nb_1 <- nb_lm(abundance = abund.28179361,
              metadata = metadata.28179361,
              pred = "diagnosis_full",
              type = "tree") 

nb_2 <- nb_lm(abundance = abund.29404425,
              metadata = metadata.29404425,
              pred = "diagnosis_full", 
              type = "tree") 
nb_1$nb$sbp <- data.frame(nb_1$nb$sbp)
nb_2$nb$sbp <- data.frame(nb_2$nb$sbp)

# compare members of balances
bal_table <- data.table(rownames(nb_1$nb$sbp), -nb_1$nb$sbp[1], -nb_2$nb$sbp[1])
colnames(bal_table) <- c("taxa","Dataset 1", "Dataset 2")
setorderv(bal_table, c("Dataset 1", "Dataset 2"))
bal_table_m <- melt(bal_table)
bal_table_m[value == -1, col := "DEN"]
bal_table_m[value == 1, col := "NUM"]
bal_table_m$taxa <- factor(bal_table_m$taxa, levels = bal_table$taxa)
bal_table_m$col <- factor(bal_table_m$col, levels = c("NUM", "DEN"))

(balances_plot <-
    ggplot(bal_table_m, aes(variable, taxa, fill = col)) + 
    geom_tile() + theme_minimal() + xlab("") + ylab("") +
    theme(legend.position = 'none') +
    geom_text(aes(label = col), size = 3))

# calculate noise and effect size, p-value
nb_1$noise 
nb_2$noise 
drop(sqrt(nb_2$lm_res$coefficients[2,] %*% nb_2$lm_res$coefficients[2,]))
drop(sqrt(nb_1$lm_res$coefficients[2,] %*% nb_1$lm_res$coefficients[2,]))
summary(manova(nb_1$lm_res))
summary(manova(nb_2$lm_res))



# plot PCA
ab <- rbind(abund.28179361, abund.29404425)
mm <- rbind(metadata.28179361[,.(diagnosis_full, sample, study)], 
            metadata.29404425[,.(diagnosis_full, sample,study)])

sbp_random <- sbp.fromRandom(ab)
ilr_random <- balance.fromSBP(ab, sbp_random)
mm <- cbind(mm, pca$x[,1:2])
mm$Dataset <- "Dataset 1"
mm[study == "pmid.29404425", Dataset := "Dataset 2"]
mm$Group <- mm$diagnosis_full

# is balance better than ilr-vector?
mm$b1 <- balance.fromSBP(ab, nb_1$nb$sbp[1])
mm$b2 <- balance.fromSBP(ab, nb_2$nb$sbp[1])
mm$v1 <- ilr_random %*% 
  t(nb_1$lm_res$coefficients[2,] %*% 
      make_psi_from_sbp(nb_1$coord$sbp) %*% 
      t(make_psi_from_sbp(sbp_random)))
mm$v2 <- ilr_random %*% 
  t(nb_2$lm_res$coefficients[2,] %*% 
      make_psi_from_sbp(nb_2$coord$sbp) %*% 
      t(make_psi_from_sbp(sbp_random)))

auc(roc(Group ~ b1, mm[Dataset == "Dataset 1"]))
auc(roc(Group ~ v1, mm[Dataset == "Dataset 1"]))

auc(roc(Group ~ b2, mm[Dataset == "Dataset 2"]))
auc(roc(Group ~ v2, mm[Dataset == "Dataset 2"]))

auc(roc(Group ~ b1, mm[Dataset == "Dataset 2"]))
auc(roc(Group ~ v1, mm[Dataset == "Dataset 2"]))

auc(roc(Group ~ b2, mm[Dataset == "Dataset 1"]))
auc(roc(Group ~ v2, mm[Dataset == "Dataset 1"]))

pca <- prcomp(ilr_random)
mm$PC1 <- pca$x[,1]
mm$PC2 <- pca$x[,2]
pr_1 <- predict(pca, unique(nb_1$sblm_summary$prediction) %*% 
                  make_psi_from_sbp(nb_1$coord$sbp) %*%
                  t(make_psi_from_sbp(sbp_random)))
pr_2 <- predict(pca, unique(nb_2$sblm_summary$prediction) %*% 
                  make_psi_from_sbp(nb_2$coord$sbp) %*%
                  t(make_psi_from_sbp(sbp_random)))
pr_lm_1 <- predict(pca, predict(nb_1$lm_res)[c(1, nrow(nb_1$coord$ilr)),] %*% 
                  make_psi_from_sbp(nb_1$coord$sbp) %*%
                  t(make_psi_from_sbp(sbp_random)))
pr_lm_2 <- predict(pca, predict(nb_2$lm_res)[c(1, nrow(nb_2$coord$ilr)),] %*% 
                  make_psi_from_sbp(nb_2$coord$sbp) %*%
                  t(make_psi_from_sbp(sbp_random)))
var_prop <- apply(pca$x,2,var)/sum(apply(pca$x,2,var))
var_prop <-round(var_prop*100, 1)
(pca_pl <- 
    ggplot(mm, aes(PC1, PC2,
                   col = Group, 
                   pch = Dataset)) + 
    geom_point() + 
    theme_minimal()  +
    theme(legend.title = element_blank(), legend.position = 'bottom') +
    scale_shape_manual(values=c(21, 8))+
    coord_fixed() +
    xlab(paste0("PC1 (", var_prop[1], "%)")) +
    ylab(paste0("PC2 (", var_prop[2], "%)")) +
    geom_segment(xend=pr_lm_1[1,"PC1"], yend = pr_lm_1[1,"PC2"],
                 x=pr_lm_1[2,"PC1"], y = pr_lm_1[2,"PC2"],
                 col = "black", size =0.3,
                 arrow = arrow(length = unit(0.05, "inches")))+
    geom_segment(xend=pr_lm_2[1,"PC1"], yend = pr_lm_2[1,"PC2"],
                 x=pr_lm_2[2,"PC1"], y = pr_lm_2[2,"PC2"], 
                 col = "black", size =0.3,
                 arrow = arrow(length = unit(0.05, "inches")))+
    geom_segment(x=pr_1[2,"PC1"], y = pr_1[2,"PC2"],
                 xend=pr_1[1,"PC1"], yend = pr_1[1,"PC2"],
                 col = "darkgreen", size =0.3, lty=2,
                 arrow = arrow(length = unit(0.05, "inches")))+
    geom_segment(x=pr_2[2,"PC1"], y = pr_2[2,"PC2"],
                 xend=pr_2[1,"PC1"], yend = pr_2[1,"PC2"], 
                 col = "darkgreen", size =0.3, lty=2,
                 arrow = arrow(length = unit(0.05, "inches"))))

all_plts <- 
  grid.arrange(
  grobs=list(pca_pl + ggtitle('A'),
             balances_plot + ggtitle('C'),
             ggdraw() + draw_image("CD/data/cv_table.png") + 
               draw_label('B',x = 0.05, y=1)),
  layout_matrix = matrix(c(1,1,1,3,2,2,2,2), ncol =2))

ggsave("CD/out/Figure7.png", all_plts, width = 9.5, height = 5.5, dpi = 500)

# Train on 28179361 and test on 29404425
mm$group <- mm$diagnosis_full
mm[,train:=ifelse(study == "pmid.28179361", T, F)]
summary_1 <- balances_summary_for_iteration(
  fitted_methods = list(
    abund = ab,
    single_balances = list(pmid.28179361 = nb_1$nb$sbp[1]),
    split = mm
  ))
summary_1_tr <- trees_summary_for_iteration(
  fitted_methods = list(
    abund = ab,
    trees = list(pmid.28179361 = nb_1$nb$sbp),
    split = mm
  ))
mm <- mm[,train:=ifelse(study == "pmid.29404425", T, F)]
summary_2 <- balances_summary_for_iteration(
  fitted_methods = list(
    abund = ab,
    single_balances = list(pmid.29404425 = nb_2$nb$sbp[1]),
    split = mm
  ))
summary_2_tr <- trees_summary_for_iteration(
  fitted_methods = list(
    abund = ab,
    trees = list(pmid.29404425 = nb_2$nb$sbp),
    split = mm
  ))


rbind(merge(summary_1, summary_1_tr),
      merge(summary_2, summary_2_tr))

# correct for systematic difference between data sets
rownames(ilr_random) <- rownames(ab)
syst_diff <- colMeans(ilr_random[mm[study =="pmid.29404425"]$sample,]) -
  colMeans(ilr_random[mm[study =="pmid.28179361"]$sample,])
ilr.29404425.corrected <- t(apply(
  ilr_random[mm[study =="pmid.29404425"]$sample, ],1,
  function(x) x - syst_diff))
clr.29404425.corrected <- ilr.29404425.corrected %*% make_psi_from_sbp(sbp_random)
abund.29404425.corrected <- exp(clr.29404425.corrected)
abund.29404425.corrected <- abund.29404425.corrected/rowSums(abund.29404425.corrected)

ab.corrected <- rbind(abund.28179361, abund.29404425.corrected)
mm$b1 <- balance.fromSBP(ab.corrected[,rownames(nb_1$nb$sbp)], nb_1$nb$sbp)[,1]
mm$b2 <- balance.fromSBP(ab.corrected[,rownames(nb_1$nb$sbp)], nb_1$nb$sbp)[,2]
mm$b3 <- balance.fromSBP(ab.corrected[,rownames(nb_1$nb$sbp)], nb_1$nb$sbp)[,1]
mm$b4 <- balance.fromSBP(ab.corrected[,rownames(nb_1$nb$sbp)], nb_1$nb$sbp)[,2]
nb_pl_1_corrected <-
  ggplot(mm, aes(b1, b2,
                 col = diagnosis_full, 
                 pch = study)) + 
  geom_point() +
  coord_fixed() + 
  scale_shape_manual(values=c(21, 8))+
  theme_minimal() 
nb_pl_2_corrected <-
  ggplot(mm, aes(b3, b4,
                 col = diagnosis_full, 
                 pch = study)) + 
  geom_point() +
  coord_fixed() + 
  scale_shape_manual(values=c(21, 8))+
  theme_minimal() 
grid.arrange(grobs = list(nb_pl_1_corrected, nb_pl_2_corrected))


ab.corrected <- rbind(abund.28179361, abund.29404425.corrected)
mm[,train:=ifelse(study == "pmid.28179361", T, F)]
summary_3 <- balances_summary_for_iteration(
  fitted_methods = list(
    abund = ab.corrected,
    single_balances = list(pmid.28179361 = nb_1$nb$sbp[colnames(ab.corrected),][1]),
    split_i = mm
  ))
mm <- mm[,train:=ifelse(study == "pmid.29404425", T, F)]
summary_4 <- balances_summary_for_iteration(
  fitted_methods = list(
    abund = ab.corrected,
    single_balances = list(pmid.29404425 = nb_2$nb$sbp[1]),
    split_i = mm
  ))

summary_1
summary_2
summary_3
summary_4