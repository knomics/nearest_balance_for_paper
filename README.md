This repository contains code for a cross-validation described in the manuscript "Approximation of a Microbiome Composition Shift by a Change in a Single Balance Between Two Groups of Taxa" V. Odinstova, N. Klimenko, A.Tyakht

1. File "nearest_balance_simulation/vector_approximation.R" contains code to plot Figure 4.

2. File "case_control_simulation/test_cv_on_sumulation.R" contains code for benchmarking on simulated data.

3. File "CD/compare_groups_CD.R" contains code for benchmarking on Crohn's disease dataset (and calculate Table 1)

4. File "plot_Figures/plot_cv_results.R" contains code to plot Figures 5 and 6.

5. File "plot_Figures/coffee_example.R" contains code for Figure 1D.

6. File "compare_nb_and_pba_constrained.R" contains comparison of the NearestBalance with the 'constrained' method in pb_basis() function of coda.base package.

